<?php  
/**
* openTeacher
*
* Open Source Software for educators. License under the GNU General Public License 
* as published by the Free Software Foundation. @see {license.txt} in the root.
*
* BEWARE! This app makes uses of AJAX with AngularJS 1.2.x. If you plan to deploy in IE, 
* try to make it IE9> and up, or just don't do anything.
*/

/*+* configuration of the server-side */
if ( file_exists( dirname(__FILE__) . '/server/config.php' ) )
	require( dirname(__FILE__) . '/server/config.php' );
	
/*+* configuration of the client-side */
if( file_exists( dirname(__FILE__) . '/client/setup.php' ) )
	require( dirname(__FILE__) . '/client/setup.php' );

/*+* Test our REST_PATH */
$app->get('/api', function () use ( $app ){
	$test = array();
	$test['api_uri'] = REST_PATH;
	echo json_encode( $test );
});


/**
* Frontend.
*
* Our client side is build in AngularJS. The main idea with this merge
* use the ability of angular to made the necessary request without 
* asking the server all the information we already brought for every URI.
* Since the URL changes but without making a full request, we are letting 
* the server side to rest (since the hash is the only thing that changes) 
* and responde only when our client ask for it. 
* Now we need to be very careful when merging, we don't want bad routes 
* and params when communicating
* 
* @link https://angularjs.org/
* @since 0.1 
*
* @return client-side app
*/
 
$app->get('/', function () { ?>

<!DOCTYPE html>
<?php
/*+*  
* Richard Blondet 
* http://richardblondet.com/ 
* Front End developer Living in Santo Domingo
*/ 
?>
<html lang="en" data-ng-app="app" data-ng-controller="MainCtrl">
<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<meta name="description" content="App for managing students the easy way.">
    <base target="_top" href="#!">
	<link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>libs/xeditable.css">
	<link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/style.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/style-md.css" media="screen and (max-width:992px)">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/style-small.css" media="screen and (max-width:768px)">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>libs/loading-bar.min.css">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>libs/angular-toastr.min.css">

	<title data-ng-bind="current_page()"></title>
    
    
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>
    <section id="navmenu">
        <nav class="navbar navbar-4teachrs navbar-fixed-top">
            <div class="container-fluid">
                <ul id="menu-toggle-container" class="nav navbar-nav navbar-left">
                    <li>
                        <a id="menu-toggle" toggle-menu>
                            <i class="md md-menu md-2x"></i> 
                        </a>
                    </li>
                </ul>
                <div id="logo-container" class="navbar-header">
                    <a href="#" class="navbar-brand">
                        <?php /* <!-- openTeacher --> */ ?>
                        <img id="logo" src="client/assets/images/logo-white.png" alt="LOGO 4TEACHERS">
                    </a>
                </div>
            </div>
        </nav>
    </section>
    <?php /** Wrapper  */ ?>
	<section id="wrapper" class="">

        <?php /* Sidebar */ ?>
        <div data-ng-include=" 'client/shared/sidebar/partials/sidebar.html' " data-ng-controller="SidebarCtrl"></div>
        <?php /* /.Sidebar */ ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            
            
            <?php /*+* ngView */ ?>
            <div data-ng-view></div>
            <?php /*+* /.end ngView */ ?>
           
            
        </div>
        <!-- /#page-content-wrapper -->

    </section>
    <!-- /#wrapper -->

	<?php /*+* load our assets and files */ ?>
	<script src="<?php echo ASSETS_PATH; ?>libs/jquery-1.10.2.js" ></script>
    <!--  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->
    <script src="<?php echo ASSETS_PATH; ?>libs/angular/angular.js" ></script> 
    <script src="<?php echo ASSETS_PATH; ?>libs/angular/angular-route.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/angular/angular-resource.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/angular/angular-animate.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/xeditable.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/loading-bar.min.js"></script>
    <script src="<?php echo ASSETS_PATH; ?>libs/angular-toastr.min.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>app.modules.js" ></script>
    <script src="<?php echo CLIENT_PATH; ?>app.routes.js" ></script>
    <script>
    	<?php /** 
    	* This is our merge with the endpoints.
    	* <?php echo REST_PATH; ?> 
    	* @link https://gist.github.com/demisx/9605099 
    	*/
    	?>
    	"use strict";
    	angular.module("app").value("version" , "0.1").config(
            function( URLProvider ){
                // Set URL
                URLProvider.setURL("http://<?php echo REST_PATH; ?>/");
                URLProvider.setAPI("http://<?php echo REST_PATH; ?>/api/");
    	   }
        );
    </script>
    <script src="<?php echo CLIENT_PATH; ?>shared/root/main-controllers.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>shared/root/main-directives.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>components/dashboard/dashboard-controller.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>shared/sidebar/sidebar-controller.js"></script>
    <?php /*+* Classrooms */  ?>
    <script src="<?php echo CLIENT_PATH; ?>components/classrooms/classroom-controllers.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>components/classrooms/classroom-services.js"></script>
    <script src="<?php echo CLIENT_PATH; ?>components/classrooms/classroom-directives.js"></script>
</body>
</html>

<? });

/*+* 
* Finally we run the app
* hoping it all works
*/ 
$app->run();

?>