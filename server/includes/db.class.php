<?php  
/**
* Database connection
*
* Right now the only thing is to access to the database, 
* TODO: configuration file for ease setting up.
*
* @package openTeacher
* @since 0.1
*/

/**
* Connects to the selected DB
*
* Provides an easy to use PDO connector. We plan to handle 
* the connections with the PDO driver only. Dunno why.
*
* @package openTeacher
* @author @richardblondet
* @see { ABSPATH config.php }
*/
class db {
	/**
	* Database name
	* @since 0.1
	* @access private
	* @var string $dbname 
	*/
	private $dbname = DB_NAME;

	/**
	* Database asignned user
	* @since 0.1
	* @access private
	* @var string $dbname 
	*/
	private $dbuser = DB_USER;


	/**
	* Database user password
	* @since 0.1
	* @access private
	* @var string $dbname 
	*/
	private $dbpass = DB_PASSWORD;

	/**
	* Host
	* @since 0.1
	* @access private
	* @var string $dbname 
	*/
	private $dbhost	= DB_HOST;

	/**
	* Connection object
	* @since 0.1
	* @access private
	* @var obj $dbh 
	*/
	private $dbh;


	/**
	* Connects to a database.
	*
	* With the params above, create a MySQL connection using a PDO driver.
	*
	* @package openTeacher
	*
	* @category server-side
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @global string $dbhost the mysql database host party.
	* @global string $dbuser mysql user.
	* @global string $dbpass the mysql database user password.
	* @global string $dbname the mysql database name.
	* @return object $dbh is the database connection.
	*/
	public function connect() {
		/**
		* Create our PDO object
		*/
		$this->dbh = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpass);

		/**
		* This configuration seems to be important.
		* @see {http://stackoverflow.com/questions/60174/how-can-i-prevent-sql-injection-in-php}
		*/
		$this->dbh->setAttribute( PDO::ATTR_EMULATE_PREPARES , false ); 
		
		/**
		* This aswell. At this point i'm afraid to ask
		*/		
		$this->dbh->setAttribute( PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION );
		
		/** The database Obj */
		return $this->dbh;
	}

	public function disconnect() {
		/* Closes our connection */
		$this->dbh = null;

		return $this->dbh;
	}
}
?>