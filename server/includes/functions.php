<?php  
/**
* Functions utils
*
* We are helding functions utilities that we have found
* over the internet, please provide the author and the link
* and what it does.
*
* @since 0.1
*
* @package openTeacher
* @subpackage server-side
*/


/**
 * Generates unique ids of randon text.
 *
 * Depending the lenght of the string and the type, generates random strings
 * for (hopefully) unique ids.
 *
 * @category server-side
 * 
 * @author Rokas Šleinius <http://https://gist.github.com/raveren>
 * @author @link https://gist.github.com/raveren
 * @since 0.1
 * @access public
 *
 * @link https://gist.github.com/raveren/5555297
 * @link http://stackoverflow.com/questions/1846202/php-how-to-generate-a-random-unique-alphanumeric-string
 *
 * @param String $type The type of the id we want.
 * @param Integer $lenght the lenght of our string
 * @return String or id
 */
function random_text( $type = 'alnum', $length = 8 ) {
	switch ( $type ) {
		case 'alnum':
			$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			break;
		case 'alpha':
			$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			break;
		case 'hexdec':
			$pool = '0123456789abcdef';
			break;
		case 'numeric':
			$pool = '0123456789';
			break;
		case 'nozero':
			$pool = '123456789';
			break;
		case 'distinct':
			$pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
			break;
		default:
			$pool = (string) $type;
			break;
	}


	$crypto_rand_secure = function ( $min, $max ) {
		$range = $max - $min;
		if ( $range < 0 ) return $min; // not so random...
		$log    = log( $range, 2 );
		$bytes  = (int) ( $log / 8 ) + 1; // length in bytes
		$bits   = (int) $log + 1; // length in bits
		$filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ( $rnd >= $range );
		return $min + $rnd;
	};

	$token = "";
	$max   = strlen( $pool );
	for ( $i = 0; $i < $length; $i++ ) {
		$token .= $pool[$crypto_rand_secure( 0, $max )];
	}
	return $token;
}



/**
* Encrypt and decrypt class.
*
* The main idea of having our information encrypted, is so,
* no 'smart-some' guy, student or friend, access to their 
* information and edit it, or modify it. But of course,
* we will let the teacher have the opportunity to change this.
*
* @see Encription::encode/Encription::decode
* @link https://gist.github.com/niczak/2501891
* @category security
*
* # Sample Call:
* 
* $str = "EsteEsLaContrasena";
* 
* $converter = new Encryption;
* $encoded = $converter->encode($str); @param String $str that is going to encrypt
* $decoded = $converter->decode($encoded); @param String $decoded the result of decryption
* 
* echo "$encoded<p>$decoded"; 
*/

class Encryption {
    var $skey = "yourSecretKey"; /* Give the teacher the option to change this. */

    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public  function encode($value){ 
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }

    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
}


?>