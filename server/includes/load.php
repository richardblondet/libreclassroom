<?php  
/**
* Load required files
*
* @since 0.1
*
* @package openTeacher
* @category server-side
*/

/* 
The path of our classroom controllers 
The path of our tasks controllers
*/
define( 'CLASSROOM_PATH' , ABSPATH . '/components/classrooms' );
define( 'TASKS_PATH' , ABSPATH . '/components/tasks' );

/*+* load the db file */
if( file_exists( ABSPATH . INC . '/db.class.php' ) )
	require( ABSPATH . INC . '/db.class.php' );

if( file_exists( ABSPATH . INC . '/functions.php' ) )
	require( ABSPATH . INC . '/functions.php' );

/*+* Initiate Slim Framework for our routes */
require( ABSPATH . INC . '/vendor/Slim/Slim.php' );
use Slim\Slim; \Slim\Slim::registerAutoloader();
$app = new Slim(array(
	'debug'		=>		DEBUG
));

/*+* load our classrooms controllers file */
if( file_exists( CLASSROOM_PATH . '/classes.class.php' ) )
	require( CLASSROOM_PATH . '/classes.class.php' );
if( file_exists( CLASSROOM_PATH . '/classes.class.php' ) )
	require( CLASSROOM_PATH . '/classes.routes.php' );

/*+* load our classroom tasks controllers */
if( file_exists( TASKS_PATH . '/tasks.class.php' ) )
	require( TASKS_PATH . '/tasks.class.php' );
if( file_exists( TASKS_PATH . '/tasks.routes.php' ) )
	require( TASKS_PATH . '/tasks.routes.php' );
?>