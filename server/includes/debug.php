<?php  
/**
* Turns on and off the debugging
*
* Depends of the DEBUG var in config.php
* @var DEBUG @type boolean
*/
if ( true === DEBUG ) {
	// Set display errors true
	// All kinds
	// Stored in errors.txt in root
	// Api erros too
	ini_set( 'display_errors' , 1 );
	error_reporting(E_ALL|E_STRICT);
	ini_set('error_log', ABSPATH . '/errors.txt');
	$app->config( 'debug', true );

} else {
		error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );
	}

?>