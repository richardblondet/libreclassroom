<?php  
/**
* Base configuration file for openTeacher {@see license.txt}
*
* In order for selfhosted openTeacher to work, we need an
* Apache server with PHP 5.4± mod_rewrite on
* MySQL database
* Wills for a better education
*
* @package openTeacher
* @since 0.1
*/

/*+* Database configurations */

/*+* MySQL name of the database */
define( 'DB_NAME' , 'openT_db' ); // testing

/*+* MySQL user with priviledges for this database */
define( 'DB_USER' , 'root' );

/*+* MySQL password of the database */
define( 'DB_PASSWORD' , 'root' );

/*+* MySQL host */
define( 'DB_HOST' , 'localhost' );

/**#@-*/

/*+* openTeacher database prefix */
$db_prefix = 'ot_';

/*+* Debugging
	 Turn on if you know what you are doing
	 Are you a developer? Do you find this code similar? Mere coincidence!
*+*/
define( 'DEBUG' , true );

/*+* Absolute path to the openTeacher directory. */
if (! defined('ABSPATH') )
	define( 'ABSPATH' , dirname(__FILE__) );

/*+* Rest path for the consumer client */
if(! defined('REST_PATH') )
	define( 'REST_PATH' , $_SERVER['SERVER_NAME'] );


/** Configure our backend thing */
if( file_exists( ABSPATH . '/settings.php' ) )
	require_once( ABSPATH . '/settings.php' );
?>