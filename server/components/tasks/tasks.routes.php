<?php  
/**
* Routes for the Tasks
*
* Define de endpoints for the Tasks 
* for our CURD operations
*
* @since 0.1
*
* @package openTeacher
* @subpackage server-side
*
* @see $app->Slim\Slim 
*/


/* Initiate the class of our classes */
$Tasks = new OT_Tasks();


/**
* Create Task.
*
* Handles the POST method for creating
* our tasks.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Tasks::create
*
* @param Object $app Our Slim obj that handles the rest app
* @param Object $Class The classroom obj of our app
* @return $response @see OT_Classes:create
*/
$app->post('/api/tasks' , function () use( $Tasks, $app ) {
    // Prepare for our incoming information
    // Sets our variable
	$request = $app->request();
	$task    = array();

	$task['header']        = $app->request->params("header");
	$task['content']       = $app->request->params("content");
	$task['grade_value']   = $app->request->params("grade_value");
	$task['due_date']      = $app->request->params("due_date");
	$task['due_time']      = $app->request->params("due_time");
	$task['class_code']    = $app->request->params("class_code");
	
	$Tasks->create( $task );
	// echo json_encode( $task ); // tested
});


/**
* Read Tasks.
*
* Handles the GET method for reading
* ALL the tasks given the class code.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Tasks::read
*
* @return $response @see OT_Tasks::read
*/
$app->get('/api/:classcode/tasks' , function( $classcode ) use ( $Tasks ) {

	// Read all tasks of that class
	$Tasks->read( $classcode ); 

});

/**
* Read single tasks.
*
* Handles the GET method for reading
* SINGLE tasks given the code.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Tasks::read
*
* @return $response @see OT_Tasks::read
*/
$app->get('/api/tasks/:code' , function( $code ) use ( $Tasks ) {

	// Read all tasks of that class
	$Tasks->single( $code ); 

});

/**
* Update a task.
*
* Handles the PUT method for updating
* a task.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Tasks::update
*
* @return $response @see OT_Classes:update
*/
$app->put('/api/tasks' , function() use ( $Tasks , $app ) {

	// Our request preparation
	$request = $app->request();
	
	// Our params
	$option  = $app->request->params("option");
	$value   = $app->request->params("value");
	$code    = $app->request->params("code");

	// the method execution
	$Tasks->update( $option , $value , $code ); 

});

/**
* Delete a task.
*
* Handles the DELETE method for uhm deleting
* a task.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::update
*
* @return $response @see OT_Classes:update
*/
$app->delete('/api/tasks' , function() use ( $Tasks , $app ) {

	// Our request preparation
	// the param request
	// Method execution
	$request = $app->request();
	$code = $app->request->params("code");
	$Tasks->delete( $code ); 
});

?>