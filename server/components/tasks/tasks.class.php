<?php  
/**
* Tasks controller
*
* The controller for the Tasks class. Here we
* handle the simple CRUDS of our task. Keeping in mind that
* in order for us to have tasks, we need classes first.
* 
*
* @since 0.1
*
* @package openTeacher
* @subpackage CLASSES
* @category server-side 
*/


/**
* Tasks.
*
* These are the simple CRUDS of this application for the classrooms tasks.
*
* @package openTeacher
*
* @category server-site
* 
* @author @richardblondet
* @since 0.1
*/
class OT_Tasks {
	
	/**
	* Insert Statement.
	*
	* @since 0.1
	* @access private
	* @var string $insert Insert statement for the tasks creations
	*/
	private $_insert = "INSERT INTO tasks( code, header, content, grade_value, due_date, due_time, class_code ) VALUES( :code, :header, :content, :grade_value, :due_date, :due_time, :class_code )";
	
	/**
	* Select Statement.
	*
	* @since 0.1
	* @access private
	* @var string $select Select all our tasks for the reading given the class
	*/
	private $_select = "SELECT * FROM tasks WHERE class_code = :class_code ORDER BY date";

	/**
	* Select Statement for single task
	*
	* @since 0.1
	* @access private
	* @var string $select Select our task for the reading
	*/
	private $_single = "SELECT * FROM tasks WHERE code = :code";

	/**
	* Delete Statement.
	*
	* @since 0.1
	* @access private
	* @var String $delete Select all our classes for the reading
	*/
	private $_delete = "DELETE FROM tasks WHERE code = :code";
	

	/**
	* Create tasks.
	*
	* Handles the creation of tasks, or homework. Whatever.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Tasks
	*
	* @param Array $tasks Gets all the tasks detail in Array. {
	* 	@param String $task['header'] The task name
	* 	@param Long String $task['content'] The task content
	*	@param String $task['grade_value'] The task grade or NULL
	*	@param String $task['due_date'] The string color of this task
	*	@param String $task['due_time'] The task due time code
	*	@param String $task['class_code'] The class code this task belongs to
	* }
	* @return Array $response The server response whether positive or negative
	*/
	public function create( $task ) {

		// Our response is Array
		// Get a unique code that is not in the db
		$response = array();
		$string = preg_replace( '/[^A-z_\-0-9]/i' , '' , $task['header'] . microtime() );
		$code   = random_text( $string , 30 );


		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_insert );

			
			$task['code'] = $code;
			// Param Binding
			// -- Bind all array params with the insert statement
			$stmt->bindParam( "code" ,  $task['code'] );
			$stmt->bindParam( "header" ,  $task['header'] );
			$stmt->bindParam( "content" ,  $task['content'] );
			$stmt->bindParam( "grade_value" ,  $task['grade_value'] );
			$stmt->bindParam( "due_date" ,  $task['due_date'] );
			$stmt->bindParam( "due_time" ,  $task['due_time'] );
			$stmt->bindParam( "class_code" ,  $task['class_code'] );

			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "Task created successfully!";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RT01";
	 			$response['message']  = "There was an error creating this task.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );
	 		

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-ET01";
			$response['message']      = "There was an error in the execution creating this task.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}

	}

	/**
	* Get all Tasks.
	*
	* List all the task in the database given the class code. 
	* This is our 'Read' of our CRUD
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Tasks
	*
	* @param String $class_code The class code to fetch al classes from
	* @return Array $response The server response whether positive or negative
	*/
	public function read( $class_code ) {

		// Our response is Array
		// If code comes empty get out
		$response = array();
		if( '' == $class_code || empty( $class_code ) ):
			echo json_encode( array( "status" => false , "message" => "No class code provided." ) );
			exit;
		endif;

		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			// Bind our only param class_code to get the task
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_select );
			$stmt->bindParam( "class_code", $class_code );


			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']  = true;
				$tasks = $stmt->fetchAll( PDO::FETCH_ASSOC );

				// Check if is comming empty
				if ( $tasks < 1 ) {
					$response['message']  = "No tasks found.";	
					$response['tasks']    = null;
				} else {
					$response['message']  = "Tasks";
					$response['tasks']    = $tasks;
				}
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RT02";
	 			$response['message']  = "There was an error while fetching all the tasks of this class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-ET02";
			$response['message']      = "There was an error while fetching the tasks of this class.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}

	}

	/**
	* Get single Tasks.
	*
	* List a task in the database given the class code
	* and given the task code. This is our 'Read' of our CRUD
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Tasks
	*
	* @param String $code The task code we are fetching
	* @return Array $response The server response whether positive or negative
	*/
	public function single( $code ) {

		// Our response is Array
		$response = array();

		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			// Bind our only param class_code to get the task
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_single );
			$stmt->bindParam( "code", $code );


			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']  = true;
				$task = $stmt->fetchAll( PDO::FETCH_ASSOC );

				// Check if is comming empty
				if ( $task < 1 ) {
					$response['message']  = "No tasks found.";	
					$response['task']    = null;
				} else {
					$response['message']  = "Tasks";
					$response['task']    = $task;
				}
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RT03";
	 			$response['message']  = "There was an error while fetching this tasks of this class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-ET03";
			$response['message']      = "There was an error while fetching the tasks of this class.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}

	}

	/**
	* Update Task.
	*
	* Update a tasks given the option you want to update
	* and the task code.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Tasks
	* 
	* @param String $option The option you want to update
	* @param Mixed $value The value of that option
	* @param String $code The class code we want to update
	* @return Array $response The server response whether positive or negative
	*/
	public function update( $option = '' , $value , $code = null ) {

		// Our responde var
		$response = array();
		
		// If option is empty string or null or false or from the dark side respond
		// If value is empty string or null or false or from the dark side respond
		// otherwise if it is one of the following, alter the SQL statement
		if( '' == $option || empty( $option ) || !isset( $option ) || null == $option || false == $option ) {
			$response['status']  = false;
			$response['message'] = "You must speficy an option to update. The current option '$option' is not valid.";
			$response['code']    = 'SS-RT04';
			echo json_encode( $response );
			exit;

		} elseif( '' == $value || empty( $value ) || !isset( $value ) || null == $value || false == $value ) {
			$response['status']  = false;
			$response['message'] = "You must speficy a value to update. The current value '$value' is not valid.";
			$response['code']    = 'SS-RT05';
			echo json_encode( $response );
			exit;

		} elseif( '' == $code || empty( $code ) || !isset( $code ) || null == $code || false == $code ) {
			$response['status']  = false;
			$response['message'] = "You must speficy what class to update. The current code '$code' is not a valid task.";
			$response['code'] = 'SS-RT06';
			echo json_encode( $response );
			exit;

		}
		else {
			switch ( $option ) {
				
				// The values that we can update are header, content, grade_value, due_date & due_time
				// Other wise there will be a negative response
				case 'header': 
					$sql = "UPDATE tasks SET header = :header WHERE code = :code";
					break;
				
				case 'content': 
					$sql = "UPDATE tasks SET content = :content WHERE code = :code";
					break;

				case 'grade_value':
					$sql = "UPDATE tasks SET grade_value = :grade_value WHERE code = :code";
					break;

				case 'due_date':
					$sql = "UPDATE tasks SET due_date = :due_date WHERE code = :code";
					break;

				case 'due_time':
					$sql = "UPDATE tasks SET due_time = :due_time WHERE code = :code";
					break;

				default:
					$response['status']  = false;
					$response['message'] = "There is not such thing like '$option' as an option. Exit...";
					$response['code'] = "SS-RT07";
					echo json_encode( $response );
					exit;
					break;
			}
		}

		// Now goes our try and catch.
		try {
			
			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $sql );

			// Param Binding
			// -- Bind all vars params with the update statement
			$stmt->bindParam( $option , $value );
			$stmt->bindParam( "code" , $code );
			
			// Run and check
			// If true let know with $responsive posivite
			// TODO: Set the success and error messages
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "The $option was updated successfully to '$value'.";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RT08";
	 			$response['message']  = "There was an error updating the '$option' of this task.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch ( PDOException $e ) {
			
			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']  = false;
			$response['message'] = "There was an error updating this task.";
			$response['code'] 	 = "SS-ET04";
			$response['description'] = $e->getMessage();

			$conn = $db->disconnect();
			echo json_encode( $response );
		}
	}

	/**
	* Delete Task.
	*
	* Delete a task given the code.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Tasks
	* 
	* @param String $code The task code you want to delete
	* @return Array $response The server response whether positive or negative
	*/
	public function delete( $code ) {

		// Our response var
		$response = array();

		// try and catch this
		try {
			
			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_delete );

			// Param Binding
			// -- Bind all vars params with the update statement
			$stmt->bindParam( "code" , $code );
			
			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "Task deleted successfully.";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RT09";
	 			$response['message']  = "There was an error deleting this task.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch ( PDOException $e ) {
			
			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']  = false;
			$response['message'] = "There was an error deleting this task.";
			$response['description'] = $e->getMessage();
			$response['code'] 	 = "SS-ET05";

			$conn = $db->disconnect();
			echo json_encode( $response );
		}
	}

}

?>
