<?php  
/**
* Clases controller
*
* The controller for the CLASSES class. Here we
* handle the simple CRUDS of our app
* 
*
* @since 0.1
*
* @package openTeacher
* @subpackage CLASSES
* @category server-side 
*/


/**
* Classes or Classrooms. Whatever.
*
* Handles the simple CRUDS of this application for the teacher classes.
*
* @package openTeacher
*
* @category server-site
* 
* @author @richardblondet
* @since 0.1
*/
class OT_Classes {
	/**
	* Insert Statement.
	*
	* @since 0.1
	* @access private
	* @var string $insert Insert statement for the creations
	*/
	private $_insert = "INSERT INTO classes(name, code, section, description, color) VALUES(:name, :code, :section, :description, :color)";
	
	/**
	* Select Statement.
	*
	* @since 0.1
	* @access private
	* @var string $select Select all our classes for the reading
	*/
	private $_select = "SELECT * FROM classes ORDER BY date";

	/**
	* Select Statement for single class
	*
	* @since 0.1
	* @access private
	* @var string $select Select our class for the reading
	*/
	private $_single = "SELECT * FROM classes WHERE code = :code";
	
	/**
	* Delete Statement.
	*
	* @since 0.1
	* @access private
	* @var string $delete Delete a class given this code
	*/
	private $_delete = "DELETE FROM classes WHERE code = :code";

	/**
	* Create classes.
	*
	* Handles the creation of the classes.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	*
	* @param Array $class Gets all the class detail in Array. {
	* 	@param String $class['name'] The class name
	*	@param String $class['code'] The class UNIQUE code
	*	@param Mixed $class['label'] The class label or NULL
	* 	@param Mixed $class['detail'] Class description or NULL
	*	@param Mixed $class['color'] The string color, of this class or NULL
	* 	@param 
	* }
	* @return Array $response The server response whether positive or negative
	*/
	public function create( $class ) {

		// Our response is Array
		// Get a unique code that is not in the db
		$response = array();
		$string = preg_replace( '/[^A-z_\-0-9]/i' , '' , $class['name'] . microtime() );
		$code   = random_text( $string , 10 );


		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_insert );

			
			$class['code'] = $code;
			// Param Binding
			// -- Bind all array params with the insert statement
			$stmt->bindParam( "name" , $class['name'] );
			$stmt->bindParam( "code" , $class['code'] );
			$stmt->bindParam( "section" , $class['section'] );
			$stmt->bindParam( "description" , $class['description'] );
			$stmt->bindParam( "color" , $class['color'] );

			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "Class created successfully!";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC01";
	 			$response['message']  = "There was an error creating class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );
	 		

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-EC01";
			$response['message']      = "There was an error in the execution to create a class.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}


	}

	/**
	* Get all Classes.
	*
	* Read all the classes in the database. It is our read 
	* of our CRUD
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	*
	* @return Array $response The server response whether positive or negative
	*/
	public function read() {

		// Our response is Array
		$response = array();

		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_select );

			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']  = true;
				$fetch = $stmt->fetchAll( PDO::FETCH_ASSOC );

				// Check if is comming empty
				if ( $fetch < 1 ) {
					$response['message']  = "No classes found.";	
					$response['classes']  = null;
				} else {
					$response['message']  = "Classes";
					$response['classes']  = $fetch;
				}
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC02";
	 			$response['message']  = "There was an error while fetching all the classes.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-EC02";
			$response['message']      = "There was an error while fetching the classes.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}

	}

	/**
	* Get sinle class.
	*
	* Read single class in the database. It is our read 
	* of our CRUD
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	*
	* @param String $code the class code
	* @return Array $response The server response whether positive or negative
	*/
	public function single( $code ) {

		// Our response is Array
		$response = array();

		if ( empty( $code ) || '' == $code || null == $code || false == $code ) {
			echo json_encode( array( "status" => false, "message" => "Must specify a class code" ) );
			exit;
		}

		// Wrap our transaction into a try and catch
		try {

			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_single );

			// Bind our param
			$stmt->bindParam( "code", $code );


			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {

				$response['status'] = true;
				$single  = $stmt->fetchAll( PDO::FETCH_ASSOC );
				
				// Check if is comming empty
				if ( $single < 1 ) {
					$response['message']  = "No classes found.";	
					$response['class']    = null;
				} else {
					$response['message']  = "Class";
					$response['class']    = $single;
				}

	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC09";
	 			$response['message']  = "There was an error while fetching this class. Class code: $code.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch( PDOException $e ) {

			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']       = false;
			$response['code']         = "SS-EC06";
			$response['message']      = "There was an error while fetching this class.";
			$response['description']  = $e->getMessage();

			// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
			echo json_encode( $response );

		}

	}



	/**
	* Update Class.
	*
	* Update a class given the option you want to update
	* and the class code.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	* 
	* @param String $option The option you want to update
	* @param Mixed $value The value of that option
	* @param String $code The class code we want to update
	* @return Array $response The server response whether positive or negative
	*/
	public function update( $option = '' , $value , $code = null ) {

		// Our responde var
		$response = array();
		
		// If option is empty string or null or false or from the dark side respond
		// If value is empty string or null or false or from the dark side respond
		// otherwise if it is one of the following, alter the SQL statement
		if( '' == $option || empty( $option ) || !isset( $option ) || null == $option || false == $option ) {
			$response['status']  = false;
			$response['message'] = "You must speficy an option to update. The current option '$option' is not valid.";
			$response['code'] = 'SS-RC03';
			echo json_encode( $response );
			exit;

		} elseif( '' == $value || empty( $value ) || !isset( $value ) || null == $value || false == $value ) {
			$response['status']  = false;
			$response['message'] = "You must speficy a value to update. The current value '$value' is not valid.";
			$response['code'] = 'SS-RC04';
			echo json_encode( $response );
			exit;

		} elseif( '' == $code || empty( $code ) || !isset( $code ) || null == $code || false == $code ) {
			$response['status']  = false;
			$response['message'] = "You must speficy what class to update. The current code '$code' is not a valid class.";
			$response['code'] = 'SS-RC05';
			echo json_encode( $response );
			exit;

		}
		else {
			switch ( $option ) {
				
				// The values that we can update are name, label, description and color
				// Other wise there will be a negative response
				case 'name': 
					$sql = "UPDATE classes SET name = :name WHERE code = :code";
					break;
				
				case 'label': 
					$sql = "UPDATE classes SET label = :label WHERE code = :code";
					break;

				case 'description':
					$sql = "UPDATE classes SET description = :description WHERE code = :code";
					break;

				case 'color':
					$sql = "UPDATE classes SET color = :color WHERE code = :code";

				default:
					$response['status']  = false;
					$response['message'] = "There is not such thing like '$option' as an option. Exit...";
					$response['code'] = 'SS-RC06';
					echo json_encode( $response );
					exit;
					break;
			}
		}

		// Now goes our try and catch.
		try {
			
			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $sql );

			// Param Binding
			// -- Bind all vars params with the update statement
			$stmt->bindParam( $option , $value );
			$stmt->bindParam( "code" , $code );
			
			// Run and check
			// If true let know with $responsive posivite
			// TODO: Set the success and error messages
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "The $option was updated successfully to $value.";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC07";
	 			$response['message']  = "There was an error updating the '$option' of this class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch ( PDOException $e ) {
			
			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']  = false;
			$response['message'] = "There was an error updating this class.";
			$response['description'] = $e->getMessage();
			$response['code'] 	 = "SS-EC03";

			$conn = $db->disconnect();
			echo json_encode( $response );
		}
	}


	/**
	* Delete Class.
	*
	* Delete a class given the code.
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	* 
	* @param String $code The class code you want to delete
	* @return Array $response The server response whether positive or negative
	*/
	public function delete( $code ) {

		// Our response var
		$response = array();

		// try and catch this
		try {
			
			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $this->_delete );

			// Param Binding
			// -- Bind all vars params with the update statement
			$stmt->bindParam( "code" , $code );
			
			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "Class deleted successfully.";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC08";
	 			$response['message']  = "There was an error deleting this class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch ( PDOException $e ) {
			
			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']  = false;
			$response['message'] = "There was an error deleting this class.";
			$response['description'] = $e->getMessage();
			$response['code'] 	 = "SS-EC04";

			$conn = $db->disconnect();
			echo json_encode( $response );
		}
	}

	/**
	* Remove a class.
	*
	* Remove a class given the code. Changes its status from
	* 'active' to 'trash'. 
	* 
	* @author @richardblondet
	* @since 0.1
	* @access public
	*
	* @see OT_Classes
	* 
	* @param String $code The class code you want to delete
	* @return Array $response The server response whether positive or negative
	*/
	public function remove( $code ) {

		// Our response var
		// our sql statement
		$response = array();
		$sql = "UPDATE classes SET status = 'trash' WHERE code = :code";

		// try and catch this
		try {
			
			// We are creating a new database obj
			// Then connect to this db through this obj
			// Prepare the statement. They say is good practice, dunno.
			$db 	= new db(); 
			$conn 	= $db->connect();
			$stmt 	= $conn->prepare( $sql );

			// Param Binding
			// -- Bind all vars params with the update statement
			$stmt->bindParam( "code" , $code );
			
			// Run and check
			// If true let know with $responsive posivite
			if( $stmt->execute() ) {
				$response['status']   = true;
				$response['message']  = "Class removed successfully.";
	 		} else {
	 			$response['status']   = false;
	 			$response['code'] 	  = "SS-RC10";
	 			$response['message']  = "There was an error removing this class.";
	 		}

	 		// Close the connection
	 		// Fire to the user our response
	 		$conn = $db->disconnect();
	 		echo json_encode( $response );

		} catch ( PDOException $e ) {
			
			// Our Exception errors
			// Closee the database 
			// Respon to client
			$response['status']  = false;
			$response['message'] = "There was an error removing this class.";
			$response['description'] = $e->getMessage();
			$response['code'] 	 = "SS-EC07";

			$conn = $db->disconnect();
			echo json_encode( $response );
		}
	}
	
	public function testing() {
		echo json_encode( array('hello' => "world" ) );
	}
}
?>
