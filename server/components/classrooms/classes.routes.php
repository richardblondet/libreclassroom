<?php 
/**
* Routes for the Classes
*
* Define de endpoints for the Classes 
* for our CURD operations
*
* @since 0.1
*
* @package openTeacher
* @subpackage server-side
*
* @see $app->Slim\Slim 
*/

/* Initiate the class of our classes */
$Class = new OT_Classes();


/**
* Create Class.
*
* Handles the POST method for creating
* our classes.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::create
*
* @param Object $app Our Slim obj that handles the rest app
* @param Object $Class The classroom obj of our app
* @return $response @see OT_Classes:create
*/
$app->post('/api/classes' , function () use( $Class, $app ) {
    // Prepare for our incoming information
    // Sets our variable
	$request = $app->request();
	$class   = array();

	$class['name']          = $app->request->params("name");
	$class['section']       = $app->request->params("section");
	$class['description']   = $app->request->params("description");
	// $class['status']        = $app->request->params("status");
	$class['color']         = $app->request->params("color");
	
	$Class->create( $class );
	// echo json_encode( $class ); // tested
});


/**
* Read Classes.
*
* Handles the GET method for reading
* ALL our classes.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::read
*
* @return $response @see OT_Classes:read
*/
$app->get('/api/classes' , function() use ( $Class ) {

	// Read all classes
	$Class->read(); 

});

/**
* Read Class.
*
* Handles the GET method for reading
* ONE of our classes.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::single
*
* @return $response @see OT_Classes:read
*/
$app->get('/api/classes/:code' , function( $code ) use ( $Class ) {

	// Read single classes
	$Class->single( $code );

});

/**
* Update our class.
*
* Handles the PUT method for updating
* a single class.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::update
*
* @return $response @see OT_Classes:update
*/
$app->put('/api/classes' , function() use ( $Class , $app ) {

	// Our request preparation
	$request = $app->request();
	
	// Our params
	$option = $app->request->params("option");
	$value  = $app->request->params("value");
	$classcode = $app->request->params("code");

	// the method execution
	$Class->update( $option , $value , $classcode ); 

});

/**
* Delete a class.
*
* Handles the DELETE method for uhm deleting
* ALL 1 class.
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::delete
*
* @return $response @see OT_Classes:delete
*/
$app->delete('/api/classes' , function() use ( $Class , $app ) {

	// Our request preparation
	// the param request
	// Method execution
	$request = $app->request();
	$classcode = $app->request->params("code");
	$Class->delete( $classcode ); 
});

/**
* Remove a class.
*
* Change the status of a class and derivades
* from 'active' to 'trash'
*
* @since 0.1
*
* @see includes/vendor/Slim\Slim:
* @see OT_Classes::remove
*
* @return $response @see OT_Classes:remove
*/
$app->delete('/api/classes/remove' , function() use ( $Class , $app ) {

	// Our request preparation
	// the param request
	// Method execution
	$request = $app->request();
	$classcode = $app->request->params("code");
	$Class->remove( $classcode ); 
});





$app->get('/api/classes/test' , function() use ( $Class , $app ) {

	// Our request preparation
	// the param request
	// Method execution
	$Class->generate_unique_code(); 
});
?>

