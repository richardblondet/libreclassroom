-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-02-2015 a las 22:35:01
-- Versión del servidor: 5.5.34
-- Versión de PHP: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `openT_db`
--
CREATE DATABASE IF NOT EXISTS `openT_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `openT_db`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `classes`
--
-- Creación: 21-02-2015 a las 13:09:19
--

CREATE TABLE IF NOT EXISTS `classes` (
`id` int(11) NOT NULL COMMENT 'class_id',
  `name` varchar(255) NOT NULL COMMENT 'class_name',
  `code` varchar(105) NOT NULL COMMENT 'class_code',
  `label` varchar(255) DEFAULT NULL COMMENT 'class_label',
  `description` text COMMENT 'class_description',
  `color` varchar(55) DEFAULT NULL COMMENT 'class_color',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'class_date'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `classes`
--

INSERT INTO `classes` (`id`, `name`, `code`, `label`, `description`, `color`, `date`) VALUES
(1, 'Sociales', '1111222334', 'SecciÃ³n 1', 'Sociales es una materia impartida por cualquiera', '#333333', '2015-02-21 21:26:36');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `classes`
--
ALTER TABLE `classes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `classes`
--
ALTER TABLE `classes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'class_id',AUTO_INCREMENT=2;