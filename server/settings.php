<?php 
/**
* Sets up the webservice
*
* Configures the webservice it works properly with the client side app
* that is going to consume it
*/

define( 'INC' , '/includes' );

header('Content-Type: text/html; charset=utf-8');

/*+* Include de files required for this setup */
require( ABSPATH . INC . '/load.php' );

/*+* Access to path/to/yourdomain/config/error-codes to check
    the errors your are getting */
$app->get('/api/config/error-codes', function() {
	$errors = file_get_contents( ABSPATH . '/error_codes.json' );
	echo ( $errors );
});

/*+* Headers to utf8 */
/* ---- ALL FUTURE SETTING HERE ----- */
/*
 TODO: 
 Build API module by module
 Build an interface for easy configuring the app
 Get the Slim configurations their own file
*/
/* ---- /. END OF SETTING ----- */

/*+* Debugging mode waiting */
if ( file_exists( ABSPATH . INC . '/debug.php' ) )
	require( ABSPATH . INC . '/debug.php' );
?>