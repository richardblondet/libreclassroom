"use strict";

/**
* Angular Module Configurations
*
* All our app configurations and dependencies.
*/

angular.module("app", [
	'ngRoute',
	'ngResource',
	'ngAnimate',
	'ui.bootstrap',
	'xeditable',
	'angular-loading-bar',
	'toastr' // for options @see https://github.com/Foxandxss/angular-toastr
])

.provider("URL" , function() {
	var API, ABSOLUTE;
	return {
		setURL: function ( value ) {
			ABSOLUTE = value;
		},
		setAPI: function ( value ) {
			API = value;
		},
		$get: function () {
			return {
				API: API,
				ABSOLUTE: ABSOLUTE
			}
		}

	}
})
/** Loading bar provider configuration */
.config(['cfpLoadingBarProvider', 
	function(cfpLoadingBarProvider) {
		cfpLoadingBarProvider.includeSpinner = false;
	}
])

.config(['cfpLoadingBarProvider', 
	function(cfpLoadingBarProvider) {
		cfpLoadingBarProvider.latencyThreshold = 100;
	}
])

/** X-editable theme option  */
.run(
	function(editableOptions) {
  		editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
	}
)