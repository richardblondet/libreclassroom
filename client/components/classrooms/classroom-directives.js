"use strict";

/**
* Our helper directives for our classes.
*
* A series of directives to add and extend functionality
* to our classes.
*/

angular.module("app")


/**
* Color Picker
*
* Gives us the colors for our classes.
* @see client/components/classroom/partials/create.html
*/
.directive('ngColorPicker', 
	function() {
		var defaultColors = [
			'#eeeeee',
			'#333333',
			'#ffffff',
			'#1abc9c',
			'#16a085',
			'#2ecc71',
			'#27ae60',
			'#3498db',
			'#2980b9',
			'#9b59b6',
			'#8e44ad',
			'#34495e',
			'#f1c40f',
			'#f39c12',
			'#e67e22',
			'#d35400',
			'#e74c3c',
			'#c0392b',
			'#00bcd4',
			'#e91e63',
			'#f8bbd0',
			'#ff4081'

		];
		// Runs during compile
		return {
			restrict: 'AE', // E = Element, A = Attribute, C = Class, M = Comment
			scope: { selected: "=", customizedColors: "=colors" },
			template: '<ul><li ng-repeat="color in colors" ng-class="{ selected: (color === selected) }" ng-click="pick(color)" style="background-color:{{ color }};" ></li></ul>',
			link: function(scope, element, attrs) {
				scope.colors    = defaultColors;
				scope.selected  = scope.selected || scope.colors[0];

				scope.pick = function( color ) {
					scope.selected = color;
				}
			}
		};
	} // End function
)

/**
* Color Picker Updater directive.
*
* We are using the same pattern for the removal directive but with
* the color picker we can find above. This is for keeping this 
* controlled. Now we could've edited the colors using the xeditable-color
* but we want to keep colors undercontrol.
*
* @see shared/root/main-directives.js: ngConfirmRemoval
*/
.directive('colorPickerUpdater', ['$modal',
	function( $modal ){
		// The modal directive
		var ModalInstanceCtrl = function($scope, $modalInstance) {
			// Close and dismiss modal
			$scope.ok = function() {
				$modalInstance.close();
			}
			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			}
			/* Default color ? */
			// $scope.color = "Helloooooo";
		};

		return {
			restrict: 'A',
			scope: {
				colorPickerUpdater: '&',
				currentColor: '@&=',
			},
			bindController: true,
			link: function(scope, element, attrs) {
				element.bind('click', function(){
					// The message as an attr
					var message = attrs.colorPickerMessage || "Update color.";
					var current = attrs.currentColor;
					console.log(current);
					

					// The modal html we are aiming to use
					var modalHtml;
					modalHtml = '<div class="modal-header update-color-dialog"><button type="button" class="close" ng-click="cancel()">×</button><h4>' + message + '</h4></div><div class="modal-body">'+ current +'<label for="classcolor"> Color Picker </label> <ng-color-picker selected="'+ current +'"></ng-color-picker><br /><input type="text" class="form-control" data-ng-model="color" disabled /></div>';
					modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()"> <span class="pull-right glyphicon glyphicon-check"></span>&nbsp; Update &nbsp;</button><button class="btn btn-default" ng-click="cancel()">Cancel</button></div>';

					// modal instance
					var modalInstance = $modal.open({
						template: modalHtml,
						controller: ModalInstanceCtrl
					});

					// The response after the click
					// Execute the function of our parent controller
					modalInstance.result.then(function(){
						scope.colorPickerUpdater({ currentColor:scope.currentColor });
					}, function(){
						// Dismissed
					});
				});
			}
		}
	} // end function
])