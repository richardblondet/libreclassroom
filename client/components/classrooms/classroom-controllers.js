"use strict";

/**
* Classrooms controllers.
*
* Our classroom controllers for all our classes
* functionality.
*
* http://vitalets.github.io/angular-xeditable/#getstarted
*/
angular.module("app")

/**
* CreateClass Controller
*
* Handles de logic for the create.html partial
*/
.controller("CreateClassCtrl" , ['$scope', 'Classes', '$window', 'toastr', 
	function(  $scope , Classes , $window , toastr ) {
		
		// Class variable for our model
		$scope.class = {
			name: "",
			section: "",
			description: "",
			color: ""
		};

		// Default color selection
		$scope.class.color  = "#eeeeee";
		$scope.errors = undefined;

		/*
		* Create class function.
		*
		* Triggered when submitting the form to
		* create a class. The form checks for the 
		* scope variable 'class'.
		*/
		$scope.createClass  = function () {

			// Our error variable inside the scope.
			$scope.errors = [];

			// Check if name is empty or has a space
			if( $scope.class.name == "" || $scope.class.name == " " || $scope.class.name == undefined  || createclassForm.name.$modelValue || createclassForm.name.$modelValue ) {
				$scope.errors.push("Please insert a name for this class.");
			}

			// Check if section is empty or has a space
			else if( $scope.class.section == "" || $scope.class.section == " " || $scope.class.section == undefined  || createclassForm.section.$modelValue ) {
				$scope.errors.push("Please specify a section for this class.");
			} 
			
			// Send the class
			else {
				
				// Check if description is empty to populate with a default message
				if( $scope.class.description == "" || $scope.class.description == " " ) {
					$scope.class.description = "No description provided";
				}
				
				// Execute the create class crud function
				Classes.create( $scope.class ).success(function(data, status) {
					// response
					var response = data;

					// positive response
					if( response.status ) {
						toastr.success( response.message, 'Success' , {
							closeButton: true
						});
						// redirect user to all classes
						$window.location = "#/classes";

					}
					
					// negative response
					else {
						toastr.warning( response.message + "\n" + response.code, 'Notice!' , {
							closeButton: true
						});
						if( response.description ) {
							console.log(response.description);
						}
					}

				});
				// console.log($scope.class);
			}
		};

		// Macth if is a valid color hex
		$scope.isValidColor = function ( str ) {
		    return ( str.match(/^#[a-f0-9]{6}$/i) !== null );
		    // console.log( ( str.match(/^#[a-f0-9]{6}$/i) !== null ) );
		}	
	} // End function
])

/**
* ClassroomCtrl Controller
*
* Handles de logic for the classes.html partial
*/
.controller("ClassroomCtrl" , ['$scope', 'ClassesTest', 'Classes', 'toastr', 'URL',
	
	function( $scope , ClassesTest , Classes , toastr , URL ) {
		// toastr.warning( "Notice message", 'Notice!' , {
		// 	closeButton: true
		// });
		// Query all the classes from the api
		$scope.url = URL;

		Classes.query().success(function(data, status) {
			
			// negative response
			if(! data.status ) {
				toastr.warning( data.message + " \n " + data.description, 'Notice!' , {
					closeButton: true
				});
			} 
			// positive response
			else {
				$scope.classes = data;
			}

		});

		$scope.remove = function(item, index) {

			// Execute the remove operation
			// cach the variable, we want to send not only the value but the param as well
			var param = { code: item.code }
			Classes.remove( param ).success(function(data, status){
				// negative response
				if(! data.status ) {
					// Error message
					toastr.warning( data.message + "\n" + data.code , 'Notice!' , {
						closeButton: true
					});
					if( data.description ) {
						console.log( data.description );
					}
				}
				// positive response
				else {
					// Remove the element
					$scope.classes.classes.splice( index, 1 );
					// Success message
					toastr.success( data.message , 'Success!' , {
						closeButton: true
					});
				}
			});	
		}
	} // End function
])

.controller('SingleClassCtrl', ['$scope', '$routeParams', 'Classes', 'URL',
	function( $scope , $routeParams , Classes , URL ){
		
		// Read single class with the url code
		Classes.read( $routeParams.code ).success(function(data, status){
			/* Check if response is positive */
			if(data.status) {
				$scope.class = data.class[0];
			} 
			/* Insert loginc for error */
			else {
				// code...
			}
		});

		$scope.url = URL;

		/* Mobile collapse options */
		$scope.isCollapsed = true;

		$scope.testing = function () {
			console.log("Works? ");
		}

	} // End function
])