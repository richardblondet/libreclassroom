"use strict";

/*
* Classroom services
*
* All our comunication with our backend system,
* api consumtion and comunication with our client
* side app, is not here, but part.
* 
* @dependency 'ngResource'
*/


angular.module("app")

/**
* Testing
*/ 
.factory("ClassesTest" , 
	function() {
		return {
			"component": "classes",
			"hello": "world",
			"foo": "bar"
		}
	} // End function
)

/**
* Classes factory
*
* Handles our operations to consume the api
*/
.factory('Classes', 
	function( $http , URL ){
		return {
			/**
			* Create classes
			*/
			create: function( obj ) {
				return $http({
					url: '/api/classes',
					method: 'POST',
					data: $.param(obj),
					headers: { 
						'Content-type': 'application/x-www-form-urlencoded'
					}
				});
			},
			/**
			* Get all classes
			*/
			query: function() {
				return $http({
					url: '/api/classes',
					method: 'GET',
					cache: true
				});
			},
			/**
			* Get single class, given the code
			*/
			read: function( code ) {
				return $http({
					url: '/api/classes/' + code,
					method: 'GET',
					cache: true
				});
			},

			/**
			* Update the class given the option
			*/
			update: function( obj ) {
				return $http({
					url: '/api/classes/',
					method: 'PUT',
					data: $.param(obj),
					headers: {
						'Content-type': 'application/x-www-form-urlencoded'
					}
				});
			},
			/**
			* Remove class. Set 'active' to 'trash'
			*/
			remove: function( code ) {
				return $http({
					url: '/api/classes/remove',
					method: 'DELETE',
					data: $.param(code),
					headers: {
						'Content-type': 'application/x-www-form-urlencoded'
					}
				});
			}
		}
	} // End Function
)

