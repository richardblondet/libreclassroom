"use strict";
/**
* App routes.
*
* Define our routes and the pages it includes. When
* including our views, we must declare alongside, in
* the corresponding directory, our controlls.
*
* we also should try these dishes: 
* - kebab
* - durum
*
*/

angular.module("app")

.config(['$routeProvider', '$locationProvider' , function( $routeProvider, $locationProvider ) {
	
	$locationProvider
		.hashPrefix("!");
	
	$routeProvider

	/*
	* Classes
	*/
	.when('/classes/create', {
		templateUrl: 'client/components/classrooms/partials/create.html',
		controller: 'CreateClassCtrl'
	})
	
	.when('/classes', {
		templateUrl: '/client/components/classrooms/partials/classes.html', 
		controller: 'ClassroomCtrl'
	})

	.when('/classes/:code', {
		templateUrl: 'client/components/classrooms/partials/single.html',
		controller: 'SingleClassCtrl'
	})

	.when('/dashboard', {
		templateUrl: '/client/components/dashboard/partials/dashboard.html', 
		controller: 'DashboardCtrl'
	})
	

	.otherwise({
		redirectTo: '/dashboard'
	});
}])