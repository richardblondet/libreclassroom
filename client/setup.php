<?php  
/**
* Client setup file
*
* We are defining path to our required files and libs 
* for easy handling in the index page. We need to remember
* that this is a single page app and we need constant
* modularity. 
*
* @link https://scotch.io/tutorials/angularjs-best-practices-directory-structure
* @since 0.1
*
* @package openTeacher
* @subpackage client-side
*/


/*+* 
* ASSETS PATH
* This is the path that helds all third party directives and libraries
* such as jQuery and Angular, wait what? 
*/ 
define( 'ASSETS_PATH' ,  '/client/assets/' );


/*+* 
* Client Path
* This is the path that helds all third party directives and libraries
* such as jQuery and Angular, wait what? 
*/ 
define( 'CLIENT_PATH' ,  '/client/' );

?>