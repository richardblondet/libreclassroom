"use strict";

/**
* Our sidebar controller.
*
* Let our sidebar know where we are navegating
* so it could assign the 'active' class to the 
* given element in our menu.
*
*/

angular.module("app").


controller("SidebarCtrl", ['$scope' , '$location',
	function( $scope , $location ) {

		/**
		* Returns 'active' if we are in the given path
		*
		* @param path
		* @return string
		*/ 
		$scope.isActive = function( path ) {
			if ( $location.path().substr(0, path.length) == path ) { 
				return "active"; 
			} else {  
				return ""; 
			} 
		};
	} 
]);