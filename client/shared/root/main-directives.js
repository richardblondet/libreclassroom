"use strict";

/**
* Main Directives
*
* Our simple, easy to use directives for the root
* are defined here. 
*/

angular.module("app")

/**
* This directive handles the 
* togglable class for the sidebar
* to show or hide.
*/
.directive("toggleMenu" , function() {

	return {
		restric: 'A',
		link: function(scope, element, attrs) {
			element.bind("click", function(){
				angular.element("#wrapper").toggleClass("toggled");
			});
		}
	}

})

/**
* Confirm removal helper.
*
* This directive help us handle the confirmation 
* for any item removal. 
* @see @link http://plnkr.co/edit/DgE5eGGmGebQfWunhqqv?p=preview
* @see StackOverflow @link http://stackoverflow.com/questions/22113456/modal-confirmation-as-an-angular-ui-directive
*
* Usage: 
* -- place the attribute ng-confirm-removal to an anchor tag
* -- insert an attr ng-confirm-message with the custom message
* -- insert an attr ng-execute-fn to link the parent controller function within the scope
*/
.directive('ngConfirmRemoval', ['$modal', 
	function( $modal ) {
		// The modal directive
		var ModalInstanceCtrl = function($scope, $modalInstance) {
			// Close and dismiss modal
			$scope.ok = function() {
				$modalInstance.close();
			}
			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			}
		};
		// Runs during compile
		return {
			restrict: 'A',
			scope: { 
				ngConfirmRemoval: "&", 
				item: "=" 
			}, 
			link: function( scope, element, attrs ) {

				// Bind the element with a click
				element.bind('click', function(){
					// The message as an attr
					var message = attrs.ngConfirmMessage || "Are you sure you want to remove this item ?";

					// The modal html we are aiming to use
					var modalHtml;
					modalHtml = '<div class="modal-header confirm-dialog"><button type="button" class="close" ng-click="cancel()">×</button><h4>Confirm Removal</h4></div><div class="modal-body">'+ message +'</div>';
					modalHtml += '<div class="modal-footer"><button class="btn btn-danger" ng-click="ok()"> <span class="pull-right glyphicon glyphicon-trash"></span>&nbsp; Remove &nbsp;</button><button class="btn btn-default" ng-click="cancel()">Cancel</button></div>';

					// modal instance
					var modalInstance = $modal.open({
						template: modalHtml,
						controller: ModalInstanceCtrl
					});

					// The response after the click
					// Execute the function of our parent controller
					modalInstance.result.then(function(){
						scope.ngConfirmRemoval({ item:scope.item });
					}, function(){
						// Dismissed
					});
					
				});
			}
		};
	}] // end of function
)

