"use strict";

/**
* Main Controller
*
* we are going to have a root controller,
* just so we can keep things under control
* between the others.
*/

angular.module("app").

controller("MainCtrl", ['$scope', '$window', '$routeParams',
	
	function( $scope , $window , $routeParams ) {
		
		/**
		* Check which of the pages we are and
		* give it a name.
		*/
		$scope.current_page = function () {
			var page = "4t | ";
			switch($window.location.hash) {
				case "#!/dashboard":
					page += "Dashboard";
					break;
				
				case "#!/classes":
					page += "Classes";
					break;

				case "#!/classes/"+$routeParams.code:
					page += "View Class";
					break;

				default:
					break;
			}

			return page;
		}
	}

]);